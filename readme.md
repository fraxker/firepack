# The FirePack #
Welcome to the official repository of the FirePack minecraft mod pack.  This mod pack was designed by FireBall1725 to be used as the modpack on his subscriber server.  The modpack is now open sourced so anyone can update it, suggest new mods, or copy it to make their own modpack base.

## Contacts
* FireBall1725
* GlassPelican
* MinecraftDevin11
* WarriorFelip

## License
* FirePack ModPack
  - (c) 2015 FireBall1725
  - [![License](https://img.shields.io/badge/License-MIT-red.svg?style=flat)](http://opensource.org/licenses/MIT)

## Downloads
Downloads of the FirePack can be gotten using the FTB 3rd party code 'FirePack' or from [FireBall1725.com](http://www.fireball1725.com/subscriber-server/).  The 'FirePack' will be launched at curse as well in the future.

## Issues
Is the mod pack crashing, have a suggestion, found a bug?  Create an issue now!

1. Make sure your issue hasn't already been answered or fixed.  Also think about whether your issue is a valid one before submitting it.
2. Go to [the issues page](https://bitbucket.org/fireballfirenation/firepack/issues?status=new&status=open)
3. Click create issue
4. Enter your Issue's title (something that summarizes your issue), and then create a detailed description of the issue.
	* To help in resolving your issues please try to include the follow if applicable:
		* The problem that is happening?
		* What was expected?
		* How to reproduce the problem?
		* Multi Player or Single Player?
		* Mod Pack version?
		* Crash log (Please make sure to use [pastebin](http://pastebin.com/) for the log file) 
		* Screen shots or Pictures of the problem
5. Click `Create Issue`, and wait for feedback!

## Contribution
Before you want to add major changes, you might want to discuss them with us first, before wasting your time.
If you are still willing to contribute to this project, you can contribute via [Pull-Request](https://www.atlassian.com/git/tutorials/making-a-pull-request/).

Here are a few things to keep in mind that will help get your PR approved.

* A PR should be focused on content.
* Use the file you are editing as a style guide.
* Consider your feature.

Getting Started

1. Fork this repository
2. Clone the fork
3. Change code base
4. Add changes to git `git add -A`
5. Commit changes to your clone `git commit -m "<summery of made changes>"`
6. Push to your fork `git push`
7. Create a Pull-Request on BitBucket
8. Wait for review

## Folder Structure
Mod Pack Structure:

* client: mods, resources, config that is client side only
* common: mods, config, scripts that is common to both client and server
* server: mods, config that is server side only

Mod Pack Additional Items:

* ftbclient: client files required for FTB such as the pack logo
* ftbserver: server files required for FTB such as the install scripts and forge server 